# How to contribute to Polybian Remastered

### **What is a bug?**

There are generally three categories of bugs that may be reported:

1. **XML Validation Errors** - Issues with the XML itself that causes CTD or stability issues with the game itself.
2. **Cosmetic Bugs** - Visual problems with the custom materials, armor, hair, etc.
3. **Balance Bugs** - Units that are too strong or too weak versus base game.

NOTE: If you're encountering balance issues and you are using other mods, you're on your own. Balance bugs will only be addresses against base game units.

**DO NOT REPORT BUGS WITH POLYBIAN REMASTERED TO POLYBIAN AS HE IS NOT RESPONSIBLE FOR THIS PROJECT**.

### **Did you find a bug?**

* **Ensure the bug was not already reported** by searching on GitLab under [Issues](https://gitlab.com/Imxset21/polybianremastered/-/issues).

* If you're unable to find an open issue addressing the problem, open a new one. Be sure to include:

1. A **title and clear description**, 
2. As much relevant information as possible, including our entire mod list,
3. A **stack trace** or an **screenshot/video** demonstrating the expected behavior that is not occurring.

### **Did you write a patch that fixes a bug?**

* Open a new GitLab pull request with the patch.

* Ensure the PR description clearly describes the problem and solution. Include the relevant issue number if applicable.

* Make sure you run `make format && make validate` locally BEFORE submitting your PR to ensure that the XML diff is validated and formatted. PRs that do not validate will likely fail CI and be summarily rejected. 

### **Contributing**

Polybian Remastered is a volunteer effort. As such, I cannot promise timely releases or bug fixes. Use at your own risk. See LICENSE for more details.

Thanks again to Polybian for making such a great mod! :heart: :heart: :heart:

