##
# Polybian Remastered
#
# @file
# @version 0.5
.SUFFIXES:
.SUFFIXES: .xml .rng .rnc
.PHONY: release clean

SHELL := /bin/sh
ZIP := $(shell command -v zip 2> /dev/null)
XMLLINT := $(shell command -v xmllint 2> /dev/null)
TRANG := $(shell command -v trang 2> /dev/null)
MOD_DIR = Modules/PolybianRemastered
DATA_DIR := $(MOD_DIR)/ModuleData
XML_CONFIGS := $(filter-out $(DATA_DIR)/schemas.xml, $(wildcard $(DATA_DIR)/*.xml))
SUBMOD_XML := $(MOD_DIR)/SubModule.xml
RNG_CONFIGS := $(wildcard .rng_schemas/*.rng)
RNC_DIR = .rnc_schemas

validate: $(XML_CONFIGS) | submodule
ifndef XMLLINT
	$(error "No xmllint in $(PATH), aborting")
endif
	@$(foreach var, $?, $(XMLLINT) --relaxng .rng_schemas/$(notdir $(var:_polybian.xml=.rng)) $(var) > /dev/null;)
	@touch validate

submodule: $(SUBMOD_XML)
	@$(XMLLINT) --relaxng .rng_schemas/$(notdir $(^:.xml=.rng)) $^ > /dev/null
	@touch submodule

PolybianRemastered.zip:	validate
ifndef ZIP
	$(error "No zip in $(PATH), aborting")
endif
	$(ZIP) -r $@ $(XML_CONFIGS) $(SUBMOD_XML)

release: PolybianRemastered.zip

regen_schema: $(RNG_CONFIGS)
ifndef TRANG
	$(error "No trang in $(PATH), aborting")
endif
	@$(foreach var, $?, $(TRANG) -I rng -O rnc $(var) $(RNC_DIR)/$(notdir $(var:.rng=.rnc)) > /dev/null;)
	@touch regen_schema

format: $(XML_CONFIGS) $(SUBMOD_XML)
	@$(foreach var, $?, $(XMLLINT) --format $(var) --output $(var) > /dev/null;)
	@touch format

clean:
	@rm -f format submodule validate regen_schema PolybianRemastered.zip

# end
