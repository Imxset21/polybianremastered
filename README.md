# Polybian Remastered

This is a *remaster* of Polybian's fantastic "Imperial Overhaul" that can be found [here on NexusMods](https://www.nexusmods.com/mountandblade2bannerlord/mods/358).

**Please support the original module creator.**

## How to Install

### Vortex

Install using [Vortex](https://www.nexusmods.com/about/vortex/).

### Manual Install (Not Recommended)

Download the ZIP file from [Releases](https://gitlab.com/Imxset21/polybianremastered/-/releases), unzip, and put it in your Modules folder.

## Versioning

Versioning follows [Semantic Versioning 2.0](https://semver.org/) and will *always* match the last Bannerlord version this mod was tested for. "Beta" and "Main" are purely informational tags and should not be used as versions.
